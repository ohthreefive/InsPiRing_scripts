## InsPiRing scripts

This repo will contain a 'collection' of scripts for my LED circle from Alex
Eames of [Raspi.tv] [1] and [RasPiO] [2].

I say 'collection.' There may just be one!

Here is [Alex's website] [3] for the LEDs.

[1]: http://raspi.tv
[2]: http://rasp.io
[3]: http://rasp.io/inspiring


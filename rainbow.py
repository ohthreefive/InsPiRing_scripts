import apa
numleds = 24
ledstrip = apa.Apa(numleds)

def rainbow():
    for i in range(0,24):
        if i > 6 or i < 17:
            ledstrip.led_set(i, 0, 0, 0, 0)
    ledstrip.led_set(0, 10, 0, 128, 0)
    ledstrip.led_set(1, 10, 255, 0, 0)
    ledstrip.led_set(2, 10, 255, 0, 0)
    ledstrip.led_set(3, 10, 130, 0, 75)
    ledstrip.led_set(4, 10, 130, 0, 75)
    ledstrip.led_set(5, 10, 255, 0, 143)
    ledstrip.led_set(6, 10, 255, 0, 143)
    ledstrip.led_set(17, 10, 0, 0, 255)
    ledstrip.led_set(18, 10, 0, 0, 255)
    ledstrip.led_set(19, 10, 0, 127, 255)
    ledstrip.led_set(20, 10, 0, 127, 255)
    ledstrip.led_set(21, 10, 0, 50, 50)
    ledstrip.led_set(22, 10, 0, 50, 50)
    ledstrip.led_set(23, 10, 0, 128, 0)
    ledstrip.write_leds()
    ledstrip.zero_leds()

while True:
    rainbow()

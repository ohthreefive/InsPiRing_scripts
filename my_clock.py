import time                    # This clock works with RasPiO InsPiRing Circle
from time import sleep         # http://rasp.io/inspiring
from datetime import datetime  # based on https://github.com/raspitv/raspio-inspiring/blob/master/clock.py
import apa
numleds = 24
ledstrip = apa.Apa(numleds)

'''This first function sets a brightness that can be written
to the LEDs depending on the time of day. Between 8pm & 7am,
brightness is 0 (off), at 7am or 7pm, brightness is 1
(minimum) and at other times it's 6.'''

def getBrightness():
    timenow = datetime.now()
    hour = int(timenow.hour)
    if hour < 7 or hour > 19:
        return 0
    elif hour == 7 or hour == 19:
        return 1
    else:
        return 6

'''At sleep times, the LED ring converts to a nightlight
(one LED on at minimum brightness in a warm yellow.)'''

def nightLight():
    x = getBrightness()
    for i in range(0,24):
        if i == 0:
            ledstrip.led_set(i, x, 0, 128, 255)
        else:
            ledstrip.led_set(i, 0, 0, 0, 0)
    ledstrip.write_leds()
    ledstrip.zero_leds()
    time.sleep(0.03)

'''This function converts 24 hour clock to 12 hour as my
daughter doesn't know the 24 hour clock yet (and it's
easier to count to 12 than 24!)'''

def getHour12():
    timenow = datetime.now()
    hour = int(timenow.hour)
    if hour > 12:
        hour = hour - 12
    return hour

'''The main function, which itself calls the above three
functions in places.'''

def clock():
    brightness = getBrightness() 
    timenow = datetime.now()         # Grab time and set variables.
    hour = int(timenow.hour)         # Divide seconds by 2.5 as
    second = int(timenow.second/2.5) # there are only 24 LEDs.
    minute = int(timenow.minute)
    if hour < 8 or hour > 19:        # Put clock in night light
        nightLight()                 # mode between 8pm & 7am
    else:
        hour = getHour12()           # At other times convert time to 12 hour
        if minute == 15 or minute == 30 or minute == 45 and second in range(0,5):
            if minute == 15:
                for i in range(0,6):
                    ledstrip.led_set(i, brightness, 0, 255, 229)
            elif minute == 30:
                for i in range(0,12):
                    ledstrip.led_set(i, brightness, 0, 255, 229)
            elif minute == 45:
                for i in range(0,18):
                    ledstrip.led_set(i, brightness, 0, 255, 229)
            ledstrip.write_leds()
            ledstrip.zero_leds()
            time.sleep(0.5)
        for i in range(0,hour):
            ledstrip.led_set(i, brightness, 131, 0, 255) # Set LEDs for hour
        ledstrip.led_set(second, brightness, 0, 255, 51) # Set one LED to represent seconds.
        ledstrip.write_leds()        # Turn LEDs on with above values.
        ledstrip.zero_leds()         # Wipe the values ready for next loop
        time.sleep(0.5)              # Chill for a bit.

'''here comes the actual 'program', which is rather short!'''

try:                             # I dont really know what a try...finally
    while True:                  # is but ive just left it from Alex's code.
        clock()                  # The while loop should run infinitely,
finally:                         # calling the clock function.
    print("/nAll LEDs off!/n")   # The last three lines are just from
    ledstrip.reset_leds()        # Alex's code
